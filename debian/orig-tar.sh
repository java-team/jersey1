#!/bin/sh -e
#
# Removes unwanted content from the upstream sources.
# Called by uscan with '--upstream-version' <version> <file>
#

VERSION=$2
TAR=../jersey1_$VERSION.orig.tar.xz
DIR=jersey1-$VERSION

mkdir -p $DIR
tar -xf $3 --strip-components=1 -C $DIR
rm $3

XZ_OPT=--best tar -c -J -v -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR
